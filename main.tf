provider "aws" {
  region     = "us-east-1"
  access_key = ""
  secret_key = "" 
}

resource "aws_instance" "web" {
  ami           = "ami-0149b2da6ceec4bb0" # imagem usada, ubuntu 20.04
  instance_type = "t2.micro"

  tags = {
    Name = "webserver"
  }
}