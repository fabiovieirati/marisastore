# marisastore



## Documentação

esse projeto tem como objetivo criar um laboratorio, simulando um cliente que possui um site e quer colocar ele em cloud usando as tecnologias de ponta no mercado visando a economia e estabilidade.

## o projeto possui!

- um site, contruido com wordpress.
- um banco de dados mysql

- o site tem os seus arquivos versionados no gitlab.

## Build
- a build do site é feita atravez de um docker-compose, é gerado o arquivos de site e o banco de dados.
- o banco de dados não está tratado nesse documento, por ser um laboradorio.

## GIT
- o projeto se encontra disponivel no repositorio abaixo para o download:
```
cd repositorio a ser criado para hospedar o site
git clone git@gitlab.com:fabiovieirati/marisastore.git

```
